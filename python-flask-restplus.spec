%global srcname flask-restplus
%global libname flask_restplus
%global pkgname %{srcname}

Name:           python-%{pkgname}
Version:        0.13.0
Release:        2
Summary:        Framework for fast, easy and documented API development with Flask
License:        BSD-3-Clause
URL:            https://github.com/noirbizarre/flask-restplus
Source0:        %pypi_source
BuildArch:      noarch

%global _description %{expand:
Fully featured framework for fast, easy and documented API development with Flask.}

%description %_description


%package -n     python3-%{pkgname}
Summary:        %{summary}
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
%{?python_provide:%python_provide python3-%{srcname}}

%description -n python3-%{pkgname} %_description


%prep
%autosetup -n %{srcname}-%{version}
rm -rf %{libname}.egg-info
rm -f %{libname}/static/files/.npmignore

%build
%py3_build

%install
%py3_install

%files -n python3-%{pkgname}
%license LICENSE
%doc README.rst CHANGELOG.rst
%{python3_sitelib}/%{libname}
%{python3_sitelib}/%{libname}-*.egg-info/


%changelog
* Tue May 10 2022 xigaoxinyan <xigaoxinyan@h-partners.com> - 0.13.0-2
- License compliance rectification

* Tue Jul 06 2021 Lianguo Wang <wanglianguo@kylinos.cn> - 0.13.0-1
- Initial package for openEuler, version 0.13.0.
